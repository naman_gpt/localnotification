//
//  ViewController.swift
//  Local_Notification
//
//  Created by Naman Gupta on 17/10/21.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var img_View: UIImageView!
    @IBOutlet weak var body_TF: UITextField!
    @IBOutlet weak var title_TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerController.delegate = self
        notification.delegate = self
        notification.requestAuthorization(options: [.alert, .badge , .sound])
        {
            (success , error) in
        }
        
        // Do any additional setup after loading the view.
    }
    
//MARK:-------------PICK IMAGE BUTTON ACTION------->>>----------
    @IBAction func pick_Img_Btn_Action(_ sender: Any)
    {
        pickerController.allowsEditing = false
        pickerController.sourceType = .photoLibrary
        present(pickerController, animated: true, completion: nil)
    }
    
    
    
//MARK:-------------btn_For_Notification_Alert------->>>----------
    @IBAction func btn_For_Notification_Alert(_ sender: Any)
    {
        if self.title_TF.text == "" {
            UIAlertController.alert(title: "Please enter title...", msg: "", target: self)
        }else if self.body_TF.text == "" {
            UIAlertController.alert(title: "Please enter body...", msg: "", target: self)
        }else{
            self.notification_Control()
        }
    }
 }


//Notification Control
extension ViewController{
    func notification_Control(){
        //Contents for notifiction
        let content = UNMutableNotificationContent()
        content.categoryIdentifier = "My Notification Identifier"
        content.title = self.title_TF.text ?? ""
        content.body = self.body_TF.text ?? ""
        content.badge = 1
        content.sound = UNNotificationSound.default
        
        content.userInfo =
            [
                "title" : self.title_TF.text ?? "",
                "body"  : self.body_TF.text ?? ""
            ]
        
        
        //file:///Users/namangupta/Library/Developer/CoreSimulator/Devices/49FB8585-641F-4820-A363-F4B8C0455382/data/Containers/Bundle/Application/CBC8DB9A-0283-4309-A8EC-D8392EC7A5C5/Local_Notification.app/download.jpeg
        
        
 

        
        //for image showing on notification banner
         let url = Bundle.main.url(forResource: "download", withExtension: "jpeg")
        let img = try! UNNotificationAttachment(identifier: "", url: url!, options: .none)
        content.attachments = [img]

        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false) // timing for notification trigger
        let identifier = "Main Identifier"

        //Request
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)


        //add request
        notification.add(request) {
            (error) in
            print("\(error?.localizedDescription)")


        }
        
        //Action notification
        let like = UNNotificationAction.init(identifier: "Like", title: "Like", options: .foreground)
        let category = UNNotificationCategory.init(identifier: content.categoryIdentifier, actions: [like], intentIdentifiers: [], options: [])
        notification.setNotificationCategories([category])
    }
}


//Notification delegate methods
extension ViewController{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let url = URL(string: "https://www.google.com/"), UIApplication.shared.canOpenURL(url) {
           if #available(iOS 10.0, *) {
              UIApplication.shared.open(url, options: [:], completionHandler: nil)
           } else {
              UIApplication.shared.openURL(url)
           }
        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
//        if let dict = response.notification.request.content.userInfo as? [AnyHashable : Any]{
//            vc.str_Lbl = dict["title"] as! String
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//IMAGE PICKER DELEGATE METHOD
extension ViewController:  UIImagePickerControllerDelegate,
                           UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController,
           didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
                let imgName = imgUrl.lastPathComponent
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)

                let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
                let data = image.pngData()! as NSData
                data.write(toFile: localPath!, atomically: true)
                //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                print(photoURL)
            img_Path = photoURL.path

            }
           //var newImage: UIImage
           //if let possibleImage = info[.editedImage] as? UIImage {
           //    newImage = possibleImage
           //} else if let possibleImage = info[.originalImage] as? UIImage {
           //    newImage = possibleImage
           //} else {
        //    return
        //}
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            img_View.contentMode = .scaleAspectFit
            img_View.image = pickedImage
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    
}
